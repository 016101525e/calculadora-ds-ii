<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Calculadora</title>
    <link rel="stylesheet" href="style.css" type="text/css">
</head>
<body>
    <form action="#" method="POST" >
        <legend>Calculadora</legend>
        <p><input type="text" name="txtNro1"/> Primer Numero</p>
        <p><input type="text" name="txtNro2"/> Segundo Numero</p>
        <br>
        <p><input type="text" name="txtNro3"/> Numero unico</p>
        <p><input type="text" name="txtNro4"/> Numero grande</p>
        <br>
        <!--Sumar, Restar, Multiplicar, Dividir, Factorial, Potencia, Seno, Tangente, Porcentaje, Raiz Cuadrada, Raiz N-esima, Inversa.-->
        <p>
            <input type="submit" name="btnSumar" value="Sumar"/>
            <input type="submit" name="btnRestar" value="Restar"/>
            <input type="submit" name="btnMultiplicar" value="Multiplicar"/>
            <input type="submit" name="btnDividir" value="Dividir"/>
            <input type="submit" name="btnFactorial" value="Factorial"/>
            <input type="submit" name="btnPotencia" value="Potencia"/>
            <br>
            <br>
            <input type="submit" name="btnSeno" value="Seno"/>
            <input type="submit" name="btnTangente" value="Tangente"/>
            <input type="submit" name="btnPorcentaje" value="Porcentaje"/>
            <input type="submit" name="btnRaizCuadrada" value="RaizCuadrada"/>
            <input type="submit" name="btnRaizNesima" value="RaizNesima"/>
            <input type="submit" name="btnInversa" value="Inversa"/>
        </p>
    </form>
    <?php
    if(isset($_POST))
    {
        // Llamar a la clase Calculadora
        include("proscesos.php");
        $d1 = $_POST['txtNro1'];
        $d2 = $_POST['txtNro2'];
        $d3 = $_POST['txtNro3'];
        $d4 = $_POST['txtNro4'];

        if(isset($_POST['btnSumar']))//Operacion Suma
        {
            $calculo = new Calculadora;
            $calculo->dp1 = $d1;
            $calculo->dp2 = $d2;
            $suma = $calculo->Sumar();
            echo "La suma de ", $d1, " y ", $d2," es: " , $suma;
        }
        elseif(isset($_POST['btnRestar']))//Operacion Resta
        {
            $calculo = new Calculadora;
            $calculo->dp1 = $d1;
            $calculo->dp2 = $d2;
            $resta = $calculo->Restar();
            echo "La resta de ", $d1, " y ", $d2," es: " , $resta;
        }
        elseif(isset($_POST['btnMultiplicar']))//Operacion de Multiplicar
        {
            $calculo = new Calculadora;
            $calculo->dp1 = $d1;
            $calculo->dp2 = $d2;
            $multiplicar = $calculo->Multiplicar();
            echo "La multiplicacion de ", $d1, " y ", $d2," es: " , $multiplicar;
        }
        elseif(isset($_POST['btnDividir']))//Operacion de Division
        {
            $calculo = new Calculadora;
            $calculo->dp1 = $d1;
            $calculo->dp2 = $d2;
            $division = $calculo->Dividir();
            echo "La division de ", $d1, " y ", $d2," es: " , $division;
        }
        elseif(isset($_POST['btnFactorial']))//Operacion Factorial
        {
            $calculo = new Calculadora;
            $calculo->dp3 = $d3;
            $fact = $calculo->Factorial();
            echo "El factorial de ", $d3, " es: " , $fact;
        }
        elseif(isset($_POST['btnPotencia']))//Operacion de Potencia
        {
            $calculo = new Calculadora;
            $calculo->dp1 = $d1;
            $calculo->dp3 = $d3;
            $potencia = $calculo->Potenciar();
            echo "La potencia de ", $d1, " elevado a ", $d3," es: " , $potencia;
        }
        elseif(isset($_POST['btnSeno']))//Operacion de Seno
        {
            $calculo = new Calculadora;
            $calculo->dp4 = $d4;
            $seno = $calculo->Seno();
            echo "El seno de ", $d4, " es: " , $seno;
        }
        elseif(isset($_POST['btnTangente']))//Operacion de Tangente
        {
            $calculo = new Calculadora;
            $calculo->dp4 = $d4;
            $tangente = $calculo->Tangente();
            echo "La tangente de ", $d4, " es: " , $tangente;
        }
        elseif(isset($_POST['btnPorcentaje']))//Operacion de Porcentaje
        {
            $calculo = new Calculadora;
            $calculo->dp3 = $d3;
            $calculo->dp4 = $d4;
            $porc = $calculo->Porcentaje();
            echo "El ", $d3, "% de ", $d4," es: " , $porc;
        }
        elseif(isset($_POST['btnRaizCuadrada']))//Operacion de Raiz Cuadrada
        {
            $calculo = new Calculadora;
            $calculo->dp3 = $d3;
            $raizcua = $calculo->RaizCuadrada();
            echo "La raiz cuadrada de ", $d3, " es: " , $raizcua;
        }
        elseif(isset($_POST['btnRaizNesima']))//Operacion de Raiz N-esima
        {
            $calculo = new Calculadora;
            $calculo->dp1 = $d1;
            $calculo->dp2 = $d2;
            $raizn = $calculo->RaizNesima();
            echo "La raiz ", $d2, " de ", $d1, " es: " , $raizn;
        }
        elseif(isset($_POST['btnInversa']))//Operacion de Invertir
        {
            $calculo = new Calculadora;
            $calculo->dp4 = $d4;
            $invert = $calculo->Inversa();
            echo "La inversa de ", $d4, " es: " , $invert;
        }
    }
    ?>
</body>
</html>