<?php
    //Clase Calculadora
    class Calculadora{
        // atributos de uso
        public $dp1;
        public $dp2;
        public $dp3;
        public $dp4;

        //Metodos de Suma
        public function Sumar()
        {
            return $this->dp1 + $this->dp2;
        }

        //Metodo de Resta
        public function Restar()
        {
            return $this->dp1 - $this->dp2;
        }

        //Metodo de Multiplicacion
        public function Multiplicar()
        {
            return $this->dp1 * $this->dp2;
        }

        //Metodo de Division
        public function Dividir()
        {
            if ($this->dp2 != 0) 
            {
                return $this->dp1 / $this->dp2;
            }
            else
            {
                return 0;
            }
        }

        //Metodo de Factorial
        private function Fact($dp3)
        {
            if($dp3 == 0)
                return 1;
            else
                return $dp3 * $this->Fact($dp3 - 1);
        }
        public function Factorial()
        {
            return $this->Fact($this->dp3);
        }

        //Metodo de Potencia
        public function Potenciar()
        {
            return (pow($this->dp1,$this->dp3));
        }

        //Metodo de Seno
        public function Seno()
        {
            return (sin(deg2rad($this->dp4)));
        }

        //Metodo de Tangente
        public function Tangente()
        {
            return (tan((($this->dp4) * pi())/180));
        }

        public function Porcentaje()
        {
            return ($this->dp3 * 100) / $this->dp4;
        }

        public function RaizCuadrada()
        {
            return sqrt($this->dp3);
        }

        public function RaizNesima()
        {
            $numero = $this->dp1;
            $raiz = $this->dp2;
            return pow($numero, (1 / $raiz));
            //return $this->dp1 ^ (1 / $this->dp2);
        }

        public function Inversa()
        {
            $valor = $this->dp4;
            $long = strlen($valor);
            $invert = '';
            for($i = $long; $i > 0; $i--)
            {
                $invert.= $valor[$i-1];
            }
            return $invert;
        }
    }
?>