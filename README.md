**Calculadora DS-II**

_Actividad realizada para ser entregada como una tarea para el curso de Desarrollo de Software II._

_Se subieron los datos utilizados para completar la actividad._


[Vista Previa](https://gitlab.com/016101525e/calculadora-ds-ii/-/blob/master/desig.png)


Datos que Puedes Ingresar como Ejemplo

_Primer Dato:_ **8**

_Segundo Dato:_ **2**


_Numero Unico:_ **4**

_Numero Grande:_ **360**


**Datos Operados**


- Suma: nro1 + nro2
- Resta: nro1 - nro2
- Multiplicacion: nro1 * nro2
- Division: nro1 / nro2
- Factorial: nro3
- Potencia: nro1 ^ nro3
- Seno: nro4
- Tangente: nro4
- Porcentaje: nro3% de nro4
- Raiz Cuadrada: nro3 ^ 2
- Raiz N-esima: nro1 ^ nro2
- Inversa: nro3
